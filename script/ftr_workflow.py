"""
workflow.py
modelisation workflow, running data processing to modelisation (and score)
"""

from model.model import *
from model.neural_network_model import *
from processing.data_processing import *
import numpy as np
import pandas as pd
from sklearn import preprocessing
from sklearn.metrics import accuracy_score
from sklearn.model_selection import GridSearchCV
from sklearn.pipeline import Pipeline

# Load datasetsp
dataframes = load_data()

# 1st pipeline - stats addition

cleaned_data = pd.concat([FeatureEngineeringStatsBefore().fit_transform(data) for data in dataframes], axis=0)
# cleaned_data = cleaned_data[cleaned_data["B365D"] > 4.1]

x_train, y_train, x_test, y_test = split_dataset(cleaned_data, 0.3, "FTR")

# 2nd pipeline
transformers = [SelectBasicFeatures(["B365H", "B365D", "B365A"] + [col for col in cleaned_data if 'mean_before' in col]),
                GetMonthYear(),
                BigGameDetector(),
                ModelTotalGoals()]

data_processing = MergeTransformers(transformers)

pipeline = Pipeline([("feature_engineering", data_processing), ("model", RandomForestClassifier(n_estimators=500))])

pipeline.fit(x_train, y_train)

y_pred = pipeline.predict(x_test)

error = evaluate_model(pipeline, "Neural Network", y_pred, y_test)



# # Other test
# new_test = FeatureEngineeringStatsBefore().fit_transform(pd.read_csv("data/E1718test.csv"))
# new_test = new_test[(~new_test["HomeTeam"].isin(["Brighton", "Huddersfield"]))]
# new_test = new_test[(~new_test["AwayTeam"].isin(["Brighton", "Huddersfield"]))]
# new_test = new_test[new_test["Referee"] != "C Kavanagh"]
# new_test = new_test[new_test["B365D"] > 4.1]

# x_test_new = new_test.drop(["FTR"], axis=1)
# y_test_new = new_test["FTR"]

# error2 = evaluate_model(pipeline, "test", x_test_new, y_test_new)
# y_pred = pipeline.predict(x_test_new)
# test = pd.DataFrame({'HomeTeam': x_test_new["HomeTeam"], 'AwayTeam': x_test_new["AwayTeam"], 'truth': y_test_new, 'pred': y_pred })[["HomeTeam", "AwayTeam","truth","pred"]]
