import pandas as pd
from sklearn.externals import joblib

class Bettor:
    def __init__(self, model, betting_strategie):
        self._model = joblib.load(model)
        self._betting_strategie = betting_strategie
        self._bets = []

    def predict(self, data):
        prediction = self._model.predict_proba(data)[0]
        return {
            "A": prediction[0],
            "D": prediction[1],
            "H": prediction[2]
        }

    def bet(self, data):
        odds = {
        "H": data["B365H"],
        "D": data["B365D"],
        "A": data["B365A"]
        }
        prediction_proba = self.predict(pd.DataFrame(data,index=[0]))  # {"home":0.8, "draw":0.1, "away":0.1}
        predicted_outcome = self._betting_strategie(prediction_proba, odds)  # {"price":3, "odd":1.7, "predict_outcome":"home"}
        return self.build_bet(prediction_proba, predicted_outcome, odds)

    def build_bet(self, prediction_proba, predicted_outcome, odds):
        odd = odds[predicted_outcome] if predicted_outcome != "None" else "None"
        price = 2
        bet = {
            "price": price,
            "odd": odd,
            "predict_outcome":predicted_outcome,
            "prediction_proba":prediction_proba
        }
        return bet

    def run(self, data):
        data = data.to_dict("records")  # deal with dict
        for line in data:
            self._bets.append(self.bet(line))


    def get_bets(self):
        return self._bets


# Betting strategies
# prediction = {"A":0.0137005, "D":0.0360136, "H":0.950286}
# odds = {"A":21.00, "D":11.00, "H":1.08}

def bs_model(prediction, odds):
    argmax_prediction = max(prediction.keys(), key=(lambda k: prediction[k]))
    return argmax_prediction if prediction[argmax_prediction] > 0.7 else "None"

def bs_draw(prediction, odds):
    argmax_prediction = max(prediction.keys(), key=(lambda k: prediction[k]))
    predict_outcome = argmax_prediction if prediction[argmax_prediction] > 0.4 and argmax_prediction=="D" else "None"
    return predict_outcome

def bs_draw_default_95_confidence(prediction, odds):
    argmax_prediction = max(prediction.keys(), key=(lambda k: prediction[k]))
    if prediction[argmax_prediction] > 0.5 and argmax_prediction=="D":
        predict_outcome = argmax_prediction
    elif prediction[argmax_prediction] > 0.95:
        predict_outcome = argmax_prediction
    else:
        predict_outcome = "None"
    return predict_outcome

def bs_valuebet(prediction, odds):
    valuebets = {}
    for outcome, odd in odds.items():
        valuebets.update({outcome:odd*prediction[outcome]})
    argmax_valuebet = max(valuebets.keys(), key=(lambda k: valuebets[k]))
    if valuebets[argmax_valuebet] > 1.5 and prediction[argmax_valuebet] > 0.7:
        return argmax_valuebet
    else:
        return "None"
