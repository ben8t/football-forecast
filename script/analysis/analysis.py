import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

# Heatmap
# heatmap(processed_data.filter(regex='^B|^FT',axis=1))  # select only betting and result score

# Random Forest Features Importances
#seaborn.barplot(x="feature_importances", y="feature", data=random_forest_feature_importances)
#random_forest_feature_importances = pd.DataFrame({"feature":list(x_train.columns),"feature_importances":model.feature_importances_})

def analysis_FTR_groupby_month(processed_data):
    result_data = pd.get_dummies(processed_data, "FTR").groupby("month", as_index=False).mean()[["month", "FTR_H", "FTR_D", "FTR_A"]]
    tidy = (
    result_data.set_index('month')
      .stack()  # un-pivots the data
      .reset_index()  # moves all data out of the index
      .rename(columns={'level_1': 'FTR', 0: 'Value'})
    )
    sns.barplot(x="month", y="Value", hue="FTR", data=tidy)
    plt.show()
