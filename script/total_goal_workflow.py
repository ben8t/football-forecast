# Predict total goals

from model.model import *
from model.neural_network_model import *
from processing.data_processing import *
import numpy as np
import pandas as pd
from sklearn import preprocessing
from sklearn.metrics import mean_absolute_error
from sklearn.model_selection import GridSearchCV

dataframes = load_data()
cleaned_data = pd.concat([FeatureEngineeringStatsBefore().fit_transform(data) for data in dataframes], axis=0)

x_train, y_train, x_test, y_test = split_dataset(cleaned_data, 0.3, "TotalGoals")

transformers = [SelectBasicFeatures(["B365H", "B365D", "B365A"]),
                GetMonthYear(),
                BigGameDetector()]
                
data_processing = MergeTransformers(transformers)

pipeline = Pipeline([("feature_engineering", data_processing),
                     ("model", model_neural_network(nn_extractor_structure_regressor,input_dim=8, epochs=1000))])

pipeline.fit(x_train, y_train)

y_pred = pipeline.predict(x_test)

print(mean_absolute_error(y_test,y_pred))
print(np.std(y_pred))
print(np.std(y_test))
