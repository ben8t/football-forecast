# data_processing.py

import os
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline, FeatureUnion, make_pipeline, make_union, clone
from sklearn.base import BaseEstimator, TransformerMixin
import calendar
import glob

def split_dataset(data, split_rate, output_variable):
    """Split dataset in 4 parts : x_train, y_train, x_test, y_test.
    Args:
            split_rate : often 0.3
            output_variable : variable to predict

    Returns:
            x_train, y_train : train dataset to fit model.
            x_test : use for predictions
            y_test : use for error calculation
    """
    train_set, test_set = train_test_split(data, test_size=split_rate, random_state=0)
    x_train = train_set.drop(output_variable, axis=1)
    y_train = train_set[output_variable]
    x_test = test_set.drop(output_variable, axis=1)
    y_test = test_set[output_variable]
    return x_train, y_train, x_test, y_test

def heatmap(df):
    """
    df is a dataframe
    show a map whith correlation
    """
    correlation = df.corr()
    seaborn.set()
    sheatmap = seaborn.heatmap(correlation, annot=True)
    plt.show()

def load_data(data_path="data/"):
    """
    load data in dataframe list
    """
    csv_files = glob.glob(data_path+"E*.csv")
    frames = [pd.read_csv(data) for data in csv_files]
    return frames

class MergeTransformers(BaseEstimator, TransformerMixin):
    """
    Feature union between pandas dataframe
    """

    def __init__(self, list_of_transformers=[]):
        self.list_of_transformers = list_of_transformers

    def transform(self, X, **transformparamn):
        concatted = pd.concat([transformer.transform(X).reset_index(drop=True)
                               for transformer in
                               self.fitted_transformers_], axis=1)
        return concatted

    def fit(self, X, y=None, **fitparams):
        self.fitted_transformers_ = []
        for transformer in self.list_of_transformers:
            fitted_trans = clone(transformer).fit(X, y=None, **fitparams)
            self.fitted_transformers_.append(fitted_trans)
        return self

class ToDummiesTransformer(BaseEstimator, TransformerMixin):
    """
    A class to transform categorical features to binaries ones
    """
    def __init__(self, to_dummies_features):
        self.to_dummies_features = to_dummies_features

    def fit(self, data, y=None):
        dummie_data = pd.get_dummies(data, columns=self.to_dummies_features, prefix=["dummie_" + x for x in self.to_dummies_features])
        self.new_columns = list(dummie_data.columns)
        return self

    def transform(self, data, y=None):
        dummie_data = pd.get_dummies(data, columns=self.to_dummies_features, prefix=["dummie_" + x for x in self.to_dummies_features])
        out_of_range_columns = np.setdiff1d(self.new_columns, list(dummie_data.columns))
        if(len(out_of_range_columns) != 0):
            for column in out_of_range_columns:
                dummie_data[column] = 0

        columns_filter = [col for col in dummie_data.columns if "dummie" in col]
        return dummie_data[columns_filter]

class PreProcessing(BaseEstimator, TransformerMixin):
    """
    A class to load data from file and compute some stats before the game (streaming computing so can be slow)
    """
    def __init__(self):
        pass

    def preprocessing(self, data):
        data["Date"] = pd.to_datetime(data["Date"], format="%d/%m/%y")
        data["match_num"] = ["match" + str(x) for x in range(0, len(data))]
        return data

    def get_result(self, row, team):
        if row["FTR"]=="H" and row["HomeTeam"]==team:
            return "W"
        elif row["FTR"]=="A" and row["AwayTeam"]==team:
            return "W"
        elif row["FTR"]=="D":
            return "D"
        else:
            return "L"

    def get_goals(self, row, team):
        if row["HomeTeam"]==team:
            return row["FTHG"]
        elif row["AwayTeam"]==team:
            return row["FTAG"]

    def get_goals_conceded(self, row, team):
        if row["HomeTeam"]==team:
            return row["FTAG"]
        elif row["AwayTeam"]==team:
            return row["FTHG"]

    def get_shots_on_target(self, row, team):
        if row["HomeTeam"]==team:
            return row["HST"]
        elif row["AwayTeam"]==team:
            return row["AST"]

    def get_shots_on_target_conceded(self, row, team):
        if row["HomeTeam"]==team:
            return row["AST"]
        elif row["AwayTeam"]==team:
            return row["HST"]

    def get_stats_before(self, team, data_before, n_past_games, team_tag):
        results = []
        goals = 0
        goals_conceced = 0
        shots_on_target = 0
        shots_on_target_conceded = 0
        for index, row in data_before.tail(n_past_games).iterrows():
            results.append(self.get_result(row, team))
            goals += self.get_goals(row, team)
            goals_conceced += self.get_goals_conceded(row, team)
            shots_on_target += self.get_shots_on_target(row, team)
            shots_on_target_conceded += self.get_shots_on_target_conceded(row, team)
        if len(results) < 5:
            return {
                "1st_game_"+team_tag:"No enough game",
                "2nd_game_"+team_tag:"No enough game",
                "3th_game_"+team_tag:"No enough game",
                "4th_game_"+team_tag:"No enough game",
                "5th_game_"+team_tag:"No enough game",
                "mean_goals_"+team_tag:-1,
                "mean_goals_conceded_"+team_tag:-1,
                "mean_shots_on_target_"+team_tag:-1,
                "mean_shots_on_target_"+team_tag:-1,
                "stats_"+team_tag:False
            }
        else:
            return {
                "1st_game_"+team_tag:results[0],
                "2nd_game_"+team_tag:results[1],
                "3th_game_"+team_tag:results[2],
                "4th_game_"+team_tag:results[3],
                "5th_game_"+team_tag:results[4],
                "mean_goals_"+team_tag:goals/n_past_games,
                "mean_goals_conceded_"+team_tag:goals_conceced/n_past_games,
                "mean_shots_on_target_"+team_tag:shots_on_target/n_past_games,
                "mean_shots_on_target_"+team_tag:shots_on_target_conceded/n_past_games,
                "stats_"+team_tag:True
            }

    def stats_builder(self, data):
        home_stats = []
        away_stats = []
        for index, row in data.iterrows():
            home_team = row["HomeTeam"]
            away_team = row["AwayTeam"]
            date = row["Date"]
            home_team_before = data[(data["Date"] < date) & ((data["HomeTeam"]==home_team)|(data["AwayTeam"]==home_team))]
            away_team_before = data[(data["Date"] < date) & ((data["HomeTeam"]==away_team)|(data["AwayTeam"]==away_team))]
            home_stats.append(self.get_stats_before(home_team,home_team_before,5,"home"))
            away_stats.append(self.get_stats_before(away_team,away_team_before,5,"away"))

        return pd.concat([pd.DataFrame(home_stats), pd.DataFrame(away_stats)], axis= 1)

    def fit(self):
        pass

    def transform(self, file_path):
        data = pd.read_csv(file_path)
        preprocessed_data = self.preprocessing(data)
        stats_builded = self.stats_builder(preprocessed_data)
        full_data = pd.concat([data, stats_builded], axis=1)[["Div","Date","HomeTeam","AwayTeam","FTR","B365H","B365D","B365A"] + list(stats_builded.columns)]
        return full_data[full_data["stats_home"]==True]

class SelectBasicFeatures(BaseEstimator, TransformerMixin):
    """
    Select features accorting to feature list
    """

    def __init__(self, features=["Div"]):
        self.features = features

    def fit(self, df, y=None):
        return self

    def transform(self, df, y=None):
        return df[self.features]

class GetMonthYear(BaseEstimator, TransformerMixin):
    """
    Just format and extract month and year from date
    """
    def __init__(self):
        pass

    def fit(self, X=None, y=None):
        return self

    def transform(self, data, y=None):
        df = pd.DataFrame()
        date = pd.to_datetime(data["Date"], format="%d/%m/%y")
        df["month"] = date.dt.month
        df["year"] = date.dt.year
        return df

class GetMeanStatsBefore(BaseEstimator, TransformerMixin):
    """
    Gather "mean" stats (like mean goals, mean shot on target, ...) computed during preprocessing
    """
    def __init__(self):
        pass

    def fit(self, X=None, y=None):
        return self

    def transform(self, data, y=None):
        features = [feature for feature in list(data.columns) if "mean" in feature]
        return data[features]

class BigGameDetector(BaseEstimator, TransformerMixin):
    """
    Brute force understanding of "big team", which often win more away games than normal
    """
    def __init__(self):
        pass

    def fit(self, X=None, y=None):
        return self

    def transform(self, data, y=None):
        df = pd.DataFrame()
        df['HomeTeamisBig'] = data["HomeTeam"].isin(["Arsenal", "Tottenham", "Man City", "Man Utd", "Chelsea", "Liverpool"])
        df['AwayTeamisBig'] = data["AwayTeam"].isin(["Arsenal", "Tottenham", "Man City", "Man Utd", "Chelsea", "Liverpool"])
        df['BigGame'] = df["HomeTeamisBig"] & df["AwayTeamisBig"]
        return df
