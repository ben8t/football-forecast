"""
neural_network_model.py
define neural network structure
"""

from keras.models import Sequential, Model
from keras.layers import Input, Dense
from keras import optimizers
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.base import BaseEstimator, TransformerMixin, RegressorMixin
from processing.data_processing import *

def model_neural_network(model_structure, input_dim, epochs=10):
    """
    Neural networks model
    """

    # Pipeline
    pipe = Pipeline([
        ('normalise', StandardScaler()),
        ('ff', KerasRegressor(build_fn=model_structure, input_dim=input_dim, epochs=epochs, batch_size=32))])

    return pipe

def nn_extractor_structure_categorical(input_dim):
    layers_size = [int(input_dim * 1.5**i) for i in range(1, 6)]
    model = Sequential([
        Dense(layers_size[0], input_dim=input_dim, activation='sigmoid'),
        Dense(layers_size[1], activation='sigmoid'),
        Dense(layers_size[2], activation='sigmoid'),
        Dense(layers_size[3], activation='sigmoid'),
        Dense(layers_size[4], activation='relu'),
        Dense(3, activation='sigmoid')])

    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    model.summary()
    return model

def nn_extractor_structure_regressor(input_dim):
    layers_size = [int(input_dim * 1.5**i) for i in range(1, 6)]
    model = Sequential([
        Dense(layers_size[0], input_dim=input_dim, activation='relu'),
        Dense(layers_size[1], activation='relu'),
        Dense(layers_size[2], activation='relu'),
        Dense(layers_size[3], activation='relu'),
        Dense(layers_size[4], activation='relu'),
        Dense(1, activation='linear')])

    model.compile(loss='mse', optimizer=optimizers.SGD(lr=0.01), metrics=['mae'])
    model.summary()
    return model

class ModelTotalGoals(BaseEstimator, RegressorMixin):

    def __init__(self):
        pass

    def fit(self, X, y):
        y_train = X["TotalGoals"]
        x_train = X.drop(["TotalGoals"], axis=1)

        transformers = [SelectBasicFeatures(["B365H", "B365D", "B365A"]),
                        GetMonthYear(),
                        BigGameDetector()]

        data_processing = MergeTransformers(transformers)

        self.pipeline = Pipeline([("feature_engineering", data_processing),
                                  ("model", model_neural_network(nn_extractor_structure_regressor, input_dim=8, epochs=1000))])

        self.pipeline.fit(x_train, y_train)

        return self

    def transform(self, X, y=None):
        y_pred = self.pipeline.predict(X)
        return pd.DataFrame({'TotalGoals': y_pred})
