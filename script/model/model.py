"""
model.py
define model function/pipeline
"""

import numpy as np
from sklearn.metrics import accuracy_score, confusion_matrix, f1_score, classification_report
from sklearn.pipeline import Pipeline, FeatureUnion, make_pipeline, make_union, clone
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier, VotingClassifier, RandomForestRegressor
from sklearn.cluster import KMeans
from sklearn.model_selection import GridSearchCV
from xgboost import XGBClassifier, XGBRegressor
from sklearn.naive_bayes import GaussianNB, BernoulliNB, MultinomialNB


def evaluate_model(model, model_name, y_pred, y_test):
    error = {}
    error['model_name'] = model_name
    #error['accuracy'] = accuracy_score(y_test, y_pred)
    error['confusion'] = confusion_matrix(y_test, y_pred)
    #error['f1_score'] = f1_score(y_test, y_pred, average='micro')
    error['report'] = classification_report(y_test, y_pred)
    print(error['model_name'])
    print(error['report'])
    return error


def ask_pred(data):
    pred = []
    games = data["HomeTeam"] + " - " + data["AwayTeam"]
    for game in games:
        print(game)
        pred.append(input(" Your prediction : "))
    return pred


def get_classes(y_pred):
    y_encoded = np.argmax(y_pred, axis=1)
    map_dict = {2: 'H', 1: 'D', 0: 'A'}
    y_pred_labeled = [map_dict[pred] for pred in y_encoded]
    return(y_pred_labeled)
